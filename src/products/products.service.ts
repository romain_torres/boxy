import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto';
import { Product } from './products.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto): Promise<Product> {
    const product = new Product();

    product.name = createProductDto.name;
    product.description = createProductDto.description;
    product.price = createProductDto.price;

    // see how to link to a store
    /* createProductDto.stores.forEach((element) => {
      product.stores.push({ id: element });
    }); */

    return this.productsRepository.save(product);
  }

  findOne(id: string): Promise<Product> {
    return this.productsRepository.findOne(id);
  }

  findFromStore(id: string): Promise<Product[]> {
    return this.productsRepository.find({
      where: {
        store: id,
      },
    });
  }

  async remove(id: string): Promise<void> {
    await this.productsRepository.delete(id);
  }
}
