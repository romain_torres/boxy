import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../auth/guards/jwt';
import { CreateProductDto } from './dto';
import { Product } from './products.entity';
import { ProductsService } from './products.service';
@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}
  @UseGuards(JwtAuthGuard)
  @Get()
  findFromStore(@Param('id') id: string): Promise<Product[]> {
    return this.productsService.findFromStore(id);
  }


  // Implement an admin to for these routes

  @Post()
  create(@Body() createProductDto: CreateProductDto): Promise<Product> {
    return this.productsService.create(createProductDto);
  }

  @Delete()
  remove(@Param('id') id: string): Promise<void> {
    return this.productsService.remove(id);
  }
}
