import { CreateUserDto } from '../dto';

export function mockCreateUser(): CreateUserDto {
  return {
    email: 'romain.torres@gmail.com',
    password: 'helloword',
  };
}
