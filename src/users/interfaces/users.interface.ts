import { Store } from 'src/stores/stores.entity';

export interface Users {
  id: number;
  email: string;
  password: string;
  selectedStore?: Store;
}
