import { CreateProductDto } from '../dto';
import { Product } from '../products.entity';

export function mockCreateProduct(): CreateProductDto {
  return {
    name: 'P1',
    description: 'desc1',
    price: 30,
    stores: [{ id: 1 }],
  };
}

export function mockSimpleProduct(): Product {
  return {
    id: 1,
    name: 'P1',
    description: 'desc1',
    price: 30,
    stores: [{ id: 1 }],
  };
}

export function mockArrayProductsSimpleStore(): Product[] {
  return [
    {
      id: 1,
      name: 'P1',
      description: 'desc1',
      price: 30,
      stores: [{ id: 1 }],
    },
    {
      id: 2,
      name: 'P2',
      description: 'desc1',
      price: 30,
      stores: [{ id: 1 }],
    },
  ];
}

export function mockArrayProductsMultipleStores(): Product[] {
  return [
    {
      id: 1,
      name: 'P1',
      description: 'desc1',
      price: 30,
      stores: [{ id: 1 }],
    },
    {
      id: 2,
      name: 'P2',
      description: 'desc1',
      price: 30,
      stores: [{ id: 2, name: 'store2' }],
    },
  ];
}
