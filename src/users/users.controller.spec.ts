import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDto } from './dto';
import { Users } from './interfaces/users.interface';
import { mockCreateUser } from './mocks';
import { UsersController } from './users.controller';
import { User } from './users.entity';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
    })
      .useMocker((token) => {
        if (token === UsersService) {
          return {
            create: jest.fn().mockImplementation((user: CreateUserDto) =>
              Promise.resolve({
                id: 1,
                email: 'romain.torres@gmail.com',
              }),
            ),
            enterStore: jest.fn().mockImplementation((user: User) =>
              Promise.resolve({
                id: 1,
                email: 'romain.torres@gmail.com',
                selectedStore: 1,
              }),
            ),
          };
        }
      })
      .compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create()', () => {
    it('should create an user', () => {
      const createUserDto: CreateUserDto = mockCreateUser();
      expect(controller.create(createUserDto)).resolves.toEqual({
        id: 1,
        email: 'romain.torres@gmail.com',
      });
      expect(service.create).toBeCalled();
    });
  });

  /* describe('enter()', () => {
    it('an user should enters a store', () => {
      const userLogged: Users = {
        id: 1,
        email: 'romain.torres@gmail.com',
        password: 'helloword',
      };
      expect(controller.enter('1')).resolves.toEqual({
        id: 1,
        email: 'romain.torres@gmail.com',
        selectedStore: 1,
      });
      expect(service.enterStore).toBeCalled();
    });
  }); */
});
