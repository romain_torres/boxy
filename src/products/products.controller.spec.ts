import { Test, TestingModule } from '@nestjs/testing';
import { CreateProductDto } from './dto';
import {
  mockArrayProductsMultipleStores,
  mockArrayProductsSimpleStore,
  mockCreateProduct,
} from './mocks';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';

const createProductDto = mockCreateProduct();

describe('ProductsController', () => {
  let controller: ProductsController;
  let service: ProductsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
    })
      .useMocker((token) => {
        if (token === ProductsService) {
          return {
            create: jest
              .fn()
              .mockImplementation((product: CreateProductDto) =>
                Promise.resolve({ id: 1, ...product }),
              ),
            findAll: jest
              .fn()
              .mockResolvedValue(mockArrayProductsMultipleStores()),
            remove: jest.fn(),
            findFromStore: jest
              .fn()
              .mockImplementation((id: string) =>
                Promise.resolve(mockArrayProductsSimpleStore()),
              ),
          };
        }
      })
      .compile();

    controller = module.get<ProductsController>(ProductsController);
    service = module.get<ProductsService>(ProductsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create()', () => {
    it('should create a product', async () => {
      controller.create(createProductDto);
      expect(controller.create(createProductDto)).resolves.toEqual({
        id: 1,
        ...createProductDto,
      });
      expect(service.create).toHaveBeenCalledWith(createProductDto);
    });
  });

  describe('findByStore()', () => {
    it('find all products from a store', () => {
      controller.findFromStore('1');
      expect(controller.findFromStore('1')).resolves.toEqual(mockArrayProductsSimpleStore());
      expect(service.findFromStore).toHaveBeenCalledWith('1');
    });
  });

  describe('remove()', () => {
    it('remove one product', () => {
      controller.remove('1');
      expect(service.remove).toHaveBeenCalled();
    });
  });
});
