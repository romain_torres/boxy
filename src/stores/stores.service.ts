import { Body, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStoreDto } from './dto';
import { Store } from './stores.entity';

@Injectable()
export class StoresService {
  constructor(
    @InjectRepository(Store)
    private storesRepository: Repository<Store>,
  ) {}

  create(store: CreateStoreDto): Promise<Store> {
    const newStore = new Store();
    newStore.description = store.description;
    newStore.name = store.name;
    return this.storesRepository.save(newStore);
  }

  async delete(storeId: number): Promise<void> {
    await this.storesRepository.delete(storeId);
  }
}
