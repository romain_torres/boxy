import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from './dto';
import { User } from './users.entity';
import { UserRepository } from './repositories/users.repository';
import * as bcrypt from 'bcrypt';
import { Users } from './interfaces/users.interface';
const rounds = 10;

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private usersCustomRepository: UserRepository,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();

    const hashedPassword = await bcrypt.hash(createUserDto.password, rounds);

    user.email = createUserDto.email;
    user.password = hashedPassword;

    try {
      const createdUser = await this.usersCustomRepository.save(user);
      user.password = undefined;
      return createdUser;
    } catch (error) {
      //uniqe_violation code 23505 from postgreql to inform it's a duplicate email
      if (error?.code === '23505') {
        throw new HttpException(
          `email : ${user.email} already exists`,
          HttpStatus.BAD_REQUEST,
        );
      }
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async connect(email: string, password: string): Promise<User | undefined> {
    return await this.usersCustomRepository.connect(email, password);
  }

  async enterStore(id: string | number): Promise<void> {
    return await this.usersCustomRepository.enter(id.toString());
  }
}
