import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsModule } from './products/products.module';
import { CartsModule } from './carts/carts.module';
import { UsersModule } from './users/users.module';
import { StoresModule } from './stores/stores.module';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';

@Module({

  controllers: [AppController],
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      autoLoadEntities: true,
      synchronize: process.env.NODE_ENV === 'development' ? true : false,
    }),
    ProductsModule,
    CartsModule,
    UsersModule,
    StoresModule,
    AuthModule,
  ]
})
export class AppModule {}
