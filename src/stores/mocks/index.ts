import { CreateStoreDto } from '../dto';
import { Store } from '../stores.entity';

export function mockCreateStoreNoData(): CreateStoreDto {
  return {};
}

export function mockCreateStoreWithData(): CreateStoreDto {
  return {
    name: 'best store',
    description: 'best store in the world',
  };
}

export function mockStoresList(): Store[] {
  return [
    {
      id: 1,
      name: 'best store',
    },
    { id: 2, name: 'average store' },
  ];
}

export function mockOneStore(): Store {
  return {
    id: 1,
    name: 'best store',
  };
}
