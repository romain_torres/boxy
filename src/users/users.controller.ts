import { Body, Controller, Param, Post, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../auth/guards/jwt';
import { CreateUserDto } from './dto';
import { User } from './users.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}
  @Post('create')
  create(@Body() createUserdto: CreateUserDto): Promise<User> {
    return this.userService.create(createUserdto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('enter/:id')
  enter(@Param('id') id: string): Promise<void> {
    return this.userService.enterStore(id);
  }
}
