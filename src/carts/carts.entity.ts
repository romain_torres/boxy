import { Product } from 'src/products/products.entity';
import { Store } from 'src/stores/stores.entity';
import { User } from 'src/users/users.entity';
import { Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Cart {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany((type) => Product, (product) => product.id)
  products: Product[];

  @OneToOne(() => User)
  @JoinColumn()
  user: User;

  @OneToOne(() => Store)
  @JoinColumn()
  store: Store;
}
