import { Store } from 'src/stores/stores.entity';

export class CreateProductDto {
  name: string;
  description: string;
  price: number;
  stores: Store[];
}
