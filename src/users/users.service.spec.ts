import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CreateUserDto } from './dto';
import { Users } from './interfaces/users.interface';
import { mockCreateUser } from './mocks';
import { UserRepository } from './repositories/users.repository';
import { User } from './users.entity';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;
  let repository: UserRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(UserRepository),
          useValue: {
            connect: jest.fn(),
            save: jest
              .fn()
              .mockImplementation((user: CreateUserDto) =>
                Promise.resolve({ email: user.email, id: 1 }),
              ),
            enter: jest.fn().mockImplementation((user: User) =>
              Promise.resolve({
                id: 1,
                email: 'romain.torres@gmail.com',
                selectedStore: 1,
              }),
            ),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repository = module.get<UserRepository>(getRepositoryToken(UserRepository));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create()', () => {
    it('shoud create an user', async () => {
      const createUserDto: CreateUserDto = mockCreateUser();
      expect(await service.create(createUserDto)).toEqual({
        email: 'romain.torres@gmail.com',
        id: 1,
      });
      expect(repository.save).toBeCalled();
    });
  });

  /* describe('enter()', () => {
    it('shoud enter to a store and update the user', async () => {
      expect(await service.enterStore('1')).toEqual({
        id: 1,
        email: 'romain.torres@gmail.com',
        selectedStore: 1,
      });
      expect(repository.enter).toBeCalled();
    });
  }); */
});
