import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto';
import {
  mockArrayProductsMultipleStores,
  mockArrayProductsSimpleStore,
  mockCreateProduct,
  mockSimpleProduct,
} from './mocks';
import { Product } from './products.entity';
import { ProductsService } from './products.service';

const createProductDto: CreateProductDto = mockCreateProduct();

describe('ProductsService', () => {
  let service: ProductsService;
  let repository: Repository<Product>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductsService,
        {
          provide: getRepositoryToken(Product),
          useValue: {
            save: jest
              .fn()
              .mockImplementation((product: CreateProductDto) =>
                Promise.resolve({ id: 1, ...product, stores: [{ id: 1 }] }),
              ),
            delete: jest.fn(),
            find: jest.fn().mockImplementation((options: any) => {
              if (options && options.where.store)
                return Promise.resolve(mockArrayProductsSimpleStore());
              else return Promise.resolve(mockArrayProductsMultipleStores());
            }),
            findOne: jest.fn().mockResolvedValue(mockSimpleProduct()),
          },
        },
      ],
    }).compile();

    service = module.get<ProductsService>(ProductsService);
    repository = module.get<Repository<Product>>(getRepositoryToken(Product));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create()', () => {
    it('should insert a product in the db', () => {
      expect(service.create(createProductDto)).resolves.toEqual({
        id: 1,
        ...createProductDto,
      });
      expect(repository.save).toBeCalled();
    });
  });

  describe('findFromStore()', () => {
    it('shoud return products from store 1', () => {
      expect(service.findFromStore('1')).resolves.toEqual(
        mockArrayProductsSimpleStore(),
      );
      expect(repository.find).toBeCalledWith({
        where: {
          store: '1',
        },
      });
    });
  });

  describe('findOne()', () => {
    it('shoud find one specific product', () => {
      expect(service.findOne('1')).resolves.toEqual(mockSimpleProduct());
      expect(repository.findOne).toBeCalledWith('1');
    });
  });

  describe('remove()', () => {
    it('should insert a product in the db', () => {
      expect(service.remove('1'));
      expect(repository.delete).toBeCalledWith('1');
    });
  });
});
