import { EntityRepository, Repository } from 'typeorm';
import { User } from './../users.entity';
import * as bcrypt from 'bcrypt';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Users } from '../interfaces/users.interface';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async connect(email: string, password: string) {
    try {
      const user = await this.createQueryBuilder('user')
        .where('user.email = :email', { email })
        .getOne();

      const isPasswordMatched = await bcrypt.compare(password, user.password);

      user.password = undefined;

      if (isPasswordMatched) {
        return Promise.resolve(user);
      }
    } catch (error) {
      throw new HttpException('Wrong credentials', HttpStatus.BAD_REQUEST);
    }
  }

  async enter(store: string) {
    try {
      /* const updateUserResult = await this.createQueryBuilder('users')
        .update(User)
        .set({ selectedStore: () => store })
        .where('id = :id', { id: req.user.id })
        .execute();

      if (updateUserResult.affected > 0) {
        return Promise.resolve(req.user);
      } */
    } catch (error) {}
  }
}
